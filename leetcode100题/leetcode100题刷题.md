# 排序算法

### 冒泡排序

给定一个数组nums1，进行冒泡排序

```c++
        // 冒泡排序
        int cnts = nums1.size();

        for(int i = 0; i < cnts - 1; i++){ // 最外层遍历的次数

            bool flag = false;
            
            for(int j = 0; j < cnts - 1 - i; j++){  // 内层比较的范围
                if(nums1.at(j) > nums1.at(j + 1)){
                    auto tmp = nums1.at(j + 1);
                    nums1.at(j + 1) = nums1.at(j);
                    nums1.at(j) = tmp;
                    flag = true;
                }
            }

            if(!flag){
                break;
            }
        }
```



# 数组篇：

## 【1】1.两数之和

给定一个整数数组 `nums` 和一个整数目标值 `target`，请你在该数组中找出 **和为目标值** *`target`* 的那 **两个** 整数，并返回它们的数组下标。

你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。

```c++
class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        /* 双重暴力求解 */
        vector<int> result;
        for (int i = 0; i < nums.size(); i++) {
            for(int j = i + 1; j < nums.size(); j++){
                if(nums[i] + nums[j] == target){
                    result = {i, j};
                }
            }
        }
        return result;
    }
};
```



## 【2】 704.二分查找

给定一个 `n` 个元素**有序的（升序）**整型数组 `nums` 和一个目标值 `target` ，写一个函数搜索 `nums` 中的 `target`，如果目标值存在返回下标，否则返回 `-1`。

```c++
class Solution {
public:
    int search(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size() - 1;

        while(left <= right){
            int mid = (left + right) * 0.5;
            if(nums[mid] < target){
                left = mid + 1;
            }else if(nums[mid] > target){
                right = mid - 1;
            }else{
                return mid;
            }
        }

        return -1;
    }
};
```



## 【3】27.移除元素——27

给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。

不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并**原地**修改输入数组。

元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。

示例 1: 给定 nums = [3,2,2,3], val = 3, 函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。 你不需要考虑数组中超出新长度后面的元素。

示例 2: 给定 nums = [0,1,2,2,3,0,4,2], val = 2, 函数应该返回新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。

总结：对一个有序容器中，需要剔除指定元素，可以使用快慢指针

​	快指针进行搜索对象，

​	慢指针负责更新数据

```c++
class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int fast_ptr = 0, slow_ptr = 0;
        while(fast_ptr < nums.size()){
            if(nums[fast_ptr] != val){
                nums[slow_ptr] = nums[fast_ptr];
                fast_ptr++;
                slow_ptr++;
            }else{
                fast_ptr++;
            }
        }
        return slow_ptr;
    }
};


class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int slow_ptr = 0;
        for(int fast_ptr = 0; fast_ptr < nums.size(); fast_ptr++){
            if(nums[fast_ptr] != val){
                nums[slow_ptr] = nums[fast_ptr];
                slow_ptr++;
            }
        }
        nums.resize(slow_ptr); // 重置原先的数组
        return slow_ptr;
    }
};
```

PS VINSMONO中的  reduceVector（）函数也有类似想法

```c++
void reduceVector(vector<cv::Point2f> &v, vector<uchar> status) {
  int j = 0;
  for (int i = 0; i < int(v.size()); i++)
    if (status[i])
      v[j++] = v[i];
  v.resize(j);
}
```



## 【4】977.有序数组的平方

给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。

**分析：可能左端和右端都是比较大的值，因此采用左右双指针处理**

**循环结束条件：左指针比右指针大**

```c++
class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) {

        vector<int> result;
        result.resize(nums.size());
        int index = nums.size() - 1;

        int left_ptr = 0, right_ptr = nums.size() - 1;

        while(left_ptr <= right_ptr){
            int left_square = nums[left_ptr] * nums[left_ptr];
            int right_square = nums[right_ptr] * nums[right_ptr];
            if(left_square >= right_square){
                result[index] = left_square;
                index--;
                left_ptr++;
            }else{
                result[index] = right_square;
                index--;
                right_ptr--;
            }
        }

        return result;

    }
};
```



【5】







## 【88】两个非递减数组进行合并新的非递减数组

给你两个按 **非递减顺序** 排列的整数数组 `nums1` 和 `nums2`，另有两个整数 `m` 和 `n` ，分别表示 `nums1` 和 `nums2` 中的元素数目。

请你 **合并** `nums2` 到 `nums1` 中，使合并后的数组同样按 **非递减顺序** 排列。

**注意：**最终，合并后数组不应由函数返回，而是存储在数组 `nums1` 中。为了应对这种情况，`nums1` 的初始长度为 `m + n`，其中前 `m` 个元素表示应合并的元素，后 `n` 个元素为 `0` ，应忽略。`nums2` 的长度为 `n` 。



解题思路:

​	Way1：直接合并新数组，调用**<u>冒泡排序</u>**

```c++
class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        // 先合并
        for(int i = 0; i < n; i++){
            nums1.at(m + i) = nums2.at(i);
        }

        // 冒泡排序
        int cnts = nums1.size();

        for(int i = 0; i < cnts - 1; i++){ // 最外层遍历的次数

            bool flag = false;

            for(int j = 0; j < cnts - 1 - i; j++){  // 内层比较的范围
                if(nums1.at(j) > nums1.at(j + 1)){
                    auto tmp = nums1.at(j + 1);
                    nums1.at(j + 1) = nums1.at(j);
                    nums1.at(j) = tmp;
                    flag = true;
                }
            }

            if(!flag){
                break;
            }
        }

    }
};
```

​	Way2：进行 **<u>双指针</u>** 操作

```
class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        std::vector<int> tmp;

        // 两个指针指向数组指定索引，分别进行贡献各自的数据
        int pointer_1 = 0, pointer_2 = 0;
        int cur_value;
        while(pointer_1 < m || pointer_2 < n ){

            if(pointer_1 == m){
                cur_value = nums2.at(pointer_2);
                pointer_2++;
            }else if(pointer_2 == n){
                cur_value = nums1.at(pointer_1);
                pointer_1++;
            }else if(nums1.at(pointer_1) <= nums2.at(pointer_2)){
                cur_value = nums1.at(pointer_1);
                pointer_1++;
            }else if(nums1.at(pointer_1) > nums2.at(pointer_2)){
                cur_value = nums2.at(pointer_2);
                pointer_2++;
            }else{
				std::cout << "Error." << std::endl;
            }

            tmp.push_back(cur_value);
        }

        nums1 = tmp;
    }
};
```



## 240 搜索二维矩阵

编写一个高效的算法来搜索 `*m* x *n*` 矩阵 `matrix` 中的一个目标值 `target` 。该矩阵具有以下特性：

- 每行的元素从左到右升序排列。
- 每列的元素从上到下升序排列。

**Way1**：使用双层遍历方式 进行目标值查找

```c++
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {

        for(const auto& row : matrix){
            for(const auto& value : row){
                if(value == target){
                    return true;
                }
            }
        }
        return false;
    }
};
```

**<u>遇见的问题</u>**：访问vector容器索引的时候，刚开始使用的是at()，at()会进行边界检查，会损耗时间



**Way2**：拆分每一行都是升序，可以每一行进行二分查找 O(m * logn)

​	二分法的写法，给定一个升序的数组 vec， 每次查找可以减少 一半行或者一半列

```c++

// case1： 确定左右两个闭合的边界 left right的取值

// case2： while 判断是 小于等于 因为等于的时候也有意义

int left = 0;
int right = row.size() - 1;

while(left <= right){
    int mid = (left + right) * 0.5;

    if(row[mid] < target){
        left = mid + 1;
    }else if(row[mid] > target){
        right = mid - 1;
    }else{
        return true;
    }
}

```



Way3： Z字形查找，充分利用二维矩阵的升序规律从右上角开始 进行比较 

```c++
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        // 【Z字查找】

        // 起始坐标、 结束判断的条件、  

        int row = 0;
        int col = matrix[0].size() - 1;

        while(row < matrix.size() && col >= 0){
            if(matrix[row][col] > target){
                col = col - 1;
            }else if(matrix[row][col] < target){
                row = row + 1;
            }else{
                return true;
            }
        }

        return false;
        
    }
};
```

Way4： 每一行进行剪枝操作，并进行二分查找

```c++
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        // 先剪枝 后二分
        for(const auto& row : matrix){
            if(row[0] > target){
                return false;
            }

            int left = 0;
            int right = row.size() - 1;

            while(left <= right){
                int mid = (left + right)  * 0.5;
                if(row[mid] < target){
                    left = mid + 1;
                }else if(row[mid] > target){
                    right = mid - 1;
                }else{
                    return true;
                }
            }
        }
        return false;
    }
};
```





# 链表篇

单向链表定义：

​	通过指针串联在一起的线性结构，每个节点由两部分组成，一个是数据部分，一个是指针部分，最后一个节点指向的nullptr

双向链表定义：

​	单链表中的指针域只能指向节点的下一个节点。

​	双链表：每一个节点有两个指针域，一个指向下一个节点，一个指向上一个节点。

​	双链表 既可以向前查询也可以向后查询。



### 单向链表的实现

```c++
struct ListNode{
	int val;
	ListNode* next;
	ListNode(value): val(value), next(nullptr){} // 节点的有参构造——最好自己实现有参构造不然系统只有默认构造
}
```



### 单向链表的增删

​	**单向链表添加节点**

```c++

```

​	**单向链表删除节点**

```c++

```



### 双向链表实现

```
struct 
```



### 
