
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        for(const auto& row : matrix){

            int left = 0;
            int right = row.size() - 1;

            while(left <= right){
                int mid = (left + right) * 0.5;

                if(row[mid] < target){
                    left = mid + 1;
                }else if(row[mid] > target){
                    right = mid - 1;
                }else{
                    return true;
                }
            }
        }

        return false;

    }
};
//leetcode submit region end(Prohibit modification and deletion)
