
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        /* 双重暴力求解 */
        vector<int> result;
        for (int i = 0; i < nums.size(); i++) {
            for(int j = i + 1; j < nums.size(); j++){
                if(nums[i] + nums[j] == target){
                    result = {i, j};
                }
            }
        }
        return result;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
