
//leetcode submit region begin(Prohibit modification and deletion)
/*class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int fast_ptr = 0, slow_ptr = 0;
        while(fast_ptr < nums.size()){
            if(nums[fast_ptr] != val){
                nums[slow_ptr] = nums[fast_ptr];
                fast_ptr++;
                slow_ptr++;
            }else{
                fast_ptr++;
            }
        }
        return slow_ptr;
    }
};*/

class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int slow_ptr = 0;
        for(int fast_ptr = 0; fast_ptr < nums.size(); fast_ptr++){
            if(nums[fast_ptr] != val){
                nums[slow_ptr] = nums[fast_ptr];
                slow_ptr++;
            }
        }
        nums.resize(slow_ptr);
        return slow_ptr;
    }
};


//leetcode submit region end(Prohibit modification and deletion)
