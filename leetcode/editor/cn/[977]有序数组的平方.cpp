
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) {

        vector<int> result;
        result.resize(nums.size());
        int index = nums.size() - 1;

        int left_ptr = 0, right_ptr = nums.size() - 1;

        while(left_ptr <= right_ptr){
            int left_square = nums[left_ptr] * nums[left_ptr];
            int right_square = nums[right_ptr] * nums[right_ptr];
            if(left_square >= right_square){
                result[index] = left_square;
                index--;
                left_ptr++;
            }else{
                result[index] = right_square;
                index--;
                right_ptr--;
            }
        }

        return result;

    }
};
//leetcode submit region end(Prohibit modification and deletion)
