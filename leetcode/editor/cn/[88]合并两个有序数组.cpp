
//leetcode submit region begin(Prohibit modification and deletion)
#include "vector"


class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        std::vector<int> tmp;

        // 两个指针指向数组，分别进行贡献各自的数据
        int pointer_1 = 0, pointer_2 = 0;
        int cur_value;
        while(pointer_1 < m || pointer_2 < n ){

            if(pointer_1 == m){
                cur_value = nums2.at(pointer_2);
                pointer_2++;
            }else if(pointer_2 == n){
                cur_value = nums1.at(pointer_1);
                pointer_1++;
            }else if(nums1.at(pointer_1) <= nums2.at(pointer_2)){
                cur_value = nums1.at(pointer_1);
                pointer_1++;
            }else if(nums1.at(pointer_1) > nums2.at(pointer_2)){
                cur_value = nums2.at(pointer_2);
                pointer_2++;
            }else{

            }

            tmp.push_back(cur_value);
        }

        nums1 = tmp;
    }
};
//leetcode submit region end(Prohibit modification and deletion)
